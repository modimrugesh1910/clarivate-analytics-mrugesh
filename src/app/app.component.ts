import {Component, OnInit, AfterViewInit, OnDestroy} from '@angular/core';
import {MatIconRegistry, MatDialog} from '@angular/material';
import {DomSanitizer} from '@angular/platform-browser';

import 'rxjs/add/operator/filter';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {AppService} from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit, OnDestroy {
  isDarkTheme = false;
  isLoading = false;
  /** Collection of subscribed variables */
  subscriptions: Subscription[] = [];

  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer, private dialog: MatDialog,
              private router: Router, private appService: AppService) {
    // To avoid XSS attacks, the URL needs to be trusted from inside of your application.
    const avatarsSafeUrl = sanitizer.bypassSecurityTrustResourceUrl('./assets/avatars.svg');

    iconRegistry.addSvgIconSetInNamespace('avatars', avatarsSafeUrl);

  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    // listen to changes on global loading notifier
    const chObsIsDataLoading: Subscription = this.appService.isDataLoading$.subscribe(
      (notifier: boolean) => {
        // update loading notifier
        if (notifier)
          this.isLoading = notifier;
        else {
          setTimeout(() => {
            this.isLoading = notifier;
          }, 1000);
        }
      }
    );
    this.subscriptions.push(chObsIsDataLoading);
  }

  /* called when component is being destroyed
   * clean memory or unsubscribe to current events to avoid memory leaks */
  ngOnDestroy() {
    for (const subscription of this.subscriptions) {
      subscription.unsubscribe();
    }
  }
}
