import {Component, OnInit, AfterViewInit, ViewChild, ElementRef, OnDestroy} from '@angular/core';
import {AppService} from '../app.service';
import {Subscription, Observable, BehaviorSubject} from 'rxjs';
import {MatPaginator, MatSort, MatTableDataSource, PageEvent} from '@angular/material';
import {Router} from '@angular/router';

import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/debounceTime';
import {IdataInterface} from '../app.interface';
import {alldata} from '../shared/alldata';

@Component({
  selector: 'app-feature-table',
  templateUrl: './feature-table.component.html',
  styleUrls: ['feature-table.component.scss'],
  preserveWhitespaces: false
})

export class FeatureTableComponent implements OnInit, AfterViewInit, OnDestroy {
  /** Collection of subscribed variables */
  subscriptions: Subscription[] = [];
  /** data container */
  data: any;
  // data: MatTableDataSource<IdataInterface> = new MatTableDataSource(alldata);
  /** display column header */
  displayedColumns = ['movie_title', 'director_name', 'actor_1_name', 'actor_2_name',
    'genres', 'language', 'content_rating', 'budget', 'title_year', 'plot_keywords', 'movie_imdb_link'];
  /** 404 error bool*/
  dataNotFound = false;
  /** view child containers */
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild('filter', {static: false}) filter: ElementRef;

  pageSizeOptions: number[] = [5, 10, 25, 100];
  length = 100;
  pageSize = 5;
  // MatPaginator Output
  pageEvent: PageEvent;

  constructor(private appService: AppService, private router: Router) {
  }

  ngOnInit() {
    const payload: any = {
      pageNo: 1,
      pageCount: 30
    };
    this.appService.fetchData(payload);

    if (this.appService.data === undefined) {
      return;
    }

    // listen to getting data
    const chData: Subscription = this.appService.data$.subscribe(
      (notifier: number) => {
        if (notifier !== 200 || this.appService.data.length === 0 || this.data === undefined) {
          this.dataNotFound = true;
          return;
        }
        // clear up variable
        this.dataNotFound = false;
        this.data = new MatTableDataSource(this.appService.data);
        this.data.paginator = this.paginator;
        this.data.sort = this.sort;
      }
    );
    this.subscriptions.push(chData);
  }

  ngAfterViewInit() {
    // listen to getting data
    const chData: Subscription = this.appService.data$.subscribe(
      (notifier: number) => {
        if (notifier !== 200 || this.appService.data.length === 0) {
          this.dataNotFound = true;
          return;
        }
        // clear up variable
        this.dataNotFound = false;
        this.data = new MatTableDataSource(this.appService.data);
        this.data.paginator = this.paginator;
        this.data.sort = this.sort;
      }
    );
    this.subscriptions.push(chData);
    // this will be used first time sorting and pagination
    if (this.appService.data.length !== 0) {
      this.data.paginator = this.paginator;
      this.data.sort = this.sort;
    }
  }

  setPageSizeOptions(setPageSizeOptionsInput: string) {
    this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
  }

  /**
   * filter data
   * @param filterValue
   */
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.data.filter = filterValue;
  }

  /***
   * fetching next page data and call api request
   */
  fetchNextPage(): void {
    const payload: any = {
      pageNo: 1,
      pageCount: 30
    };

    payload.pageNo = this.appService.data.length / 30 + 1;
    this.appService.fetchData(payload);
  }

  /* called when component is being destroyed
   * clean memory or unsubscribe to current events to avoid memory leaks */
  ngOnDestroy() {
    for (const subscription of this.subscriptions) {
      subscription.unsubscribe();
    }
  }
}
