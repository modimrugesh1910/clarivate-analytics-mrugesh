# Clarivate Analytics

## Development Build

1. `npm install`
2. `npm start`

## Production Build

1. `npm run build`

## contact details 

email - modimrugesh1910@gmail.com
phone - 7990782930

## Live Demo
https://clarivate-mrugesh-challenge.netlify.com/#/feature-table

## Github 
https://bitbucket.org/modimrugesh1910/clarivate-analytics-mrugesh/src

## work done

* fetching data over http call
* sharing data over files
* sorting functionality
* searching functionality
* pagination
* theme changing (by toggling button on top right corner will change theme)
* version control
* deployment of code

## TODO List

1. Fix sorting on only alloted items not on all existing items.

